[English](./README.md) | 中文

# 介绍

基于阿里云 `aliyunmq/mq-http-sdk` 修改支持hyperf框架的rocketmq sdk。

## 安装

通过 [composer](https://getcomposer.org) 安装：

```sh
composer require uncleqiu/hyperf-rocketmq-sdk
```

通过 `composer.json`

```json
{
    "require": {
      "uncleqiu/hyperf-rocketmq-sdk": "dev-master"
    }
}
```

然后运行

```sh
composer install
```

## 发布配置

```shell
php bin/hyperf.php vendor:publish uncleqiu/hyperf-rocketmq-sdk
```

## 示例

### 发送普通消息
```php
<?php
    $messageData = [
        'name' => 'uncleqiu',
    ];
    (new \Uncleqiu\RocketMQ\Client())->push('topic_key1', $messageData);

```

### 消费消息

使用以下命令生成自定义命令行
```sh
php bin/hyperf.php gen:command TestConsumeCommand
```

编写消费逻辑代码
```php
<?php

declare(strict_types=1);

namespace App\Command;

use Hyperf\Command\Command as HyperfCommand;
use Hyperf\Command\Annotation\Command;
use Psr\Container\ContainerInterface;
use Uncleqiu\RocketMQ\Client;

#[Command]
class TestConsumeCommand extends HyperfCommand
{
    public function __construct(protected ContainerInterface $container)
    {
        parent::__construct('consume:topic_one');
    }

    public function configure()
    {
        parent::configure();
        $this->setDescription('Consume the rocketmq data');
    }

    public function handle()
    {
        $this->line('Begin Consume....', 'info');
        (new Client())->consume('topic_key1', $this);
    }

    // rocketmq data consume processing logic
    public function handlerMessage($message)
    {
        $mqData = json_decode($message->getMessageBody(), true);
        // write your consume logic......
        var_dump($mqData);
    }
}

```

最后，运行命令行

```sh
php bin/hyperf.php consume:topic_one
```