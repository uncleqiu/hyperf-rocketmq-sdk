English | [中文](./README-CN.md)

# Introduction

Based on Alibaba Cloud `aliyunmq/mq-http-sdk`, the rocketmq sdk that supports the hyperf framework.

## Installation

Use [composer](https://getcomposer.org) to install hyperf-rocketmq-sdk into your project:

```sh
composer require uncleqiu/hyperf-rocketmq-sdk
```

use `composer.json`

```json
{
    "require": {
      "uncleqiu/hyperf-rocketmq-sdk": "dev-master"
    }
}
```
and run
```sh
composer install
```

## Configuration

```shell
php bin/hyperf.php vendor:publish uncleqiu/hyperf-rocketmq-sdk
```

## Example

### Send a message
```php
<?php
    $messageData = [
        'name' => 'uncleqiu',
    ];
    (new \Uncleqiu\RocketMQ\Client())->push('topic_key1', $messageData);

```

### Consume message

You can run this command to generate a custom command line

```sh
php bin/hyperf.php gen:command TestConsumeCommand
```

And then write your consume logic code

```php
<?php

declare(strict_types=1);

namespace App\Command;

use Hyperf\Command\Command as HyperfCommand;
use Hyperf\Command\Annotation\Command;
use Psr\Container\ContainerInterface;
use Uncleqiu\RocketMQ\Client;

#[Command]
class TestConsumeCommand extends HyperfCommand
{
    public function __construct(protected ContainerInterface $container)
    {
        parent::__construct('consume:topic_one');
    }

    public function configure()
    {
        parent::configure();
        $this->setDescription('Consume the rocketmq data');
    }

    public function handle()
    {
        $this->line('Begin Consume....', 'info');
        (new Client())->consume('topic_key1', $this);
    }

    // rocketmq data consume processing logic
    public function handlerMessage($message)
    {
        $mqData = json_decode($message->getMessageBody(), true);
        // write your consume logic......
        var_dump($mqData);
    }
}

```

Finally, run your custom command line

```sh
php bin/hyperf.php consume:topic_one
```