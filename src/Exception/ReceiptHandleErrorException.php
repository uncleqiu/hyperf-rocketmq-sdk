<?php

namespace Uncleqiu\RocketMQ\Exception;

class ReceiptHandleErrorException extends MQException
{
}