<?php

namespace Uncleqiu\RocketMQ\Exception;

class TopicNotExistException extends MQException
{
}