<?php

namespace Uncleqiu\RocketMQ\Exception;

class MessageNotExistException extends MQException
{
}