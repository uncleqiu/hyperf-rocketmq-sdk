<?php

namespace Uncleqiu\RocketMQ\Exception;

class InvalidArgumentException extends MQException
{
}