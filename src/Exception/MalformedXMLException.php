<?php

namespace Uncleqiu\RocketMQ\Exception;

class MalformedXMLException extends MQException
{
}