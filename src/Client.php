<?php

declare(strict_types=1);

namespace Uncleqiu\RocketMQ;

use Hyperf\Collection\Arr;
use Hyperf\Context\Context;
use Hyperf\Contract\ConfigInterface;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Guzzle\PoolHandler;
use Hyperf\Process\ProcessManager;
use InvalidArgumentException;
use Uncleqiu\RocketMQ\Exception\AckMessageException;
use Uncleqiu\RocketMQ\Exception\MessageNotExistException;
use Uncleqiu\RocketMQ\Exception\MessageResolveException;
use Uncleqiu\RocketMQ\Model\TopicMessage;

class Client
{

    #[Inject]
    protected ConfigInterface $configInterface;

    private array $allowParams = [
        'config',
        'options',
        'topics',
        'host',
        'accessId',
        'secretKey',
        'instanceId',
        'mqConfig',
        'client',
        'consumer',
    ];

    public function __construct(string $name = 'default')
    {
        $key = sprintf('rocketmq.%s', $name);
        if (!$this->configInterface->has($key)) {
            throw new InvalidArgumentException(sprintf('config[%s] is not exist!', $key));
        }
        $this->initParams($this->configInterface->get($key));
        $this->client = new MQClient($this->host, $this->accessId, $this->secretKey, NULL, $this->mqConfig);
    }

    /**
     * 初始化参数
     * @param $config
     * @return void
     */
    protected function initParams($config)
    {
        $this->config = $config;
        $this->options = Arr::get($config, 'options', []);
        $this->topics = Arr::get($config, 'topics', []);
        $this->host = $config['host'];
        $this->accessId = $config['accessId'];
        $this->secretKey = $config['secretKey'];
        $this->instanceId = $config['instanceId'];
        $mqConfig = new Config();
        if (!empty($config['guzzleHandler']) && $config['guzzleHandler'] == PoolHandler::class) {
            $this->options['connect_timeout'] && $mqConfig->setConnectTimeout($this->options['connect_timeout']);
            $this->options['wait_timeout'] && $mqConfig->setRequestTimeout($this->options['wait_timeout']);
            //$mqConfig->setHandler(make(PoolHandler::class), ['option' => $this->options]);
        }
        $this->mqConfig = $mqConfig;
    }

    public function getProducer(string $topic): MQProducer
    {
        return $this->client->getProducer($this->instanceId, $topic);
    }

    public function getConsumer(string $topic, string $groupId, string $messageTag = NULL): MQConsumer
    {
        return $this->client->getConsumer($this->instanceId, $topic, $groupId, $messageTag);
    }

    /**
     * 推送消息
     * @param string $key 配置文件topics下一级key
     * @param mixed $body 推送至MQ的数据
     * @throws \Exception
     */
    public function push(string $key, mixed $body)
    {
        $topicInfo = $this->topics[$key] ?: [];
        if (!$topicInfo) throw new \Exception("topic is not exists!");

        $publishMessage = new TopicMessage(json_encode($body)); // 消息实体
        $publishMessage->putProperty('retry_count', 0); // 自定义属性 重试次数
        !empty($topicInfo['messageKey']) && $publishMessage->setMessageKey($topicInfo['messageKey']); // 设置消息key
        !empty($topicInfo['tag']) && $publishMessage->setMessageTag($topicInfo['tag']); // 设置消息tag
        !empty($topicInfo['delay']) && $topicInfo['delay'] > 0 && $publishMessage->setMessageTag($topicInfo['delay']); // 设置延迟时间
        try {
            return $this->getProducer($topicInfo['topic'])->publishMessage($publishMessage);
        } catch (\Exception $e) {
            // TODO: 记录错误日志
            throw $e;
        }
    }

    /**
     * 消费消息
     * @param string $key 配置文件topics下一级key
     * @param mixed $consumeCommand 命令行
     * @param bool $isCoroutine 是否启用协程并发消费
     * @throws \Throwable
     */
    public function consume(string $key, $consumeCommand, bool $isCoroutine = false)
    {
        $topicInfo = $this->topics[$key] ?: [];
        if (!$topicInfo) throw new \Exception("topic is not exists!");

        $consumer = $this->getConsumer($topicInfo['topic'], $topicInfo['groupId'] ?? '', $topicInfo['messageTag'] ?? NULL);
        $this->consumer = $consumer;

        while (ProcessManager::isRunning()) {
            try {
                $messages = $consumer->consumeMessage(
                    $topicInfo['numOfMessages'] ?? 3, // 一次最多消费3条(最多可设置为16条)
                    $topicInfo['waitSeconds'] ?? 3 // 长轮询时间3秒（最多可设置为30秒）
                );
            } catch (MessageResolveException $e) {
                // 当出现消息Body存在不合法字符，无法解析的时候，会抛出此异常。
                // 可以正常解析的消息列表。
                $messages = $e->getPartialResult()->getMessages();
                // 无法正常解析的消息列表。
                $failMessages = $e->getPartialResult()->getFailResolveMessages();

                $receiptHandles = [];
                foreach ($messages as $message) {
                    // 处理业务逻辑。
                    $receiptHandles[] = $message->getReceiptHandle();
                    printf("MsgID %s\n", $message->getMessageId());
                }
                foreach ($failMessages as $failMessage) {
                    // 处理存在不合法字符，无法解析的消息。
                    $receiptHandles[] = $failMessage->getReceiptHandle();
                    printf("Fail To Resolve Message. MsgID %s\n", $failMessage->getMessageId());
                }
                $this->ackMessages($receiptHandles);
                continue;
            } catch (\Throwable $e) {
                if ($e instanceof MessageNotExistException) continue;
                throw $e;
            }

            $receiptHandles = [];
            if ($isCoroutine) {
                $callback = [];
                foreach ($messages as $key => $message) {
                    $callback[$key] = function () use ($message, $consumeCommand) {
                        $consumeCommand->handleMessage($message);
                        return $message->getReceiptHandle();
                    };
                }
                $receiptHandles = parallel($callback);
            } else {
                foreach ($messages as $message) {
                    $consumeCommand->handleMessage($message);
                    $receiptHandles[] = $message->getReceiptHandle();
                }
            }
            $this->ackMessages($receiptHandles);
        }

    }

    /**
     * 消息确认
     * @param $receiptHandles
     * @return void
     */
    public function ackMessages($receiptHandles)
    {
        try {
            $this->consumer->ackMessage($receiptHandles);
        } catch (\Exception $e) {
            if ($e instanceof AckMessageException) { // TODO:记录日志
                // 某些消息的句柄可能超时，会导致消费确认失败。
                printf("Ack Error, RequestId:%s\n", $e->getRequestId());
                foreach ($e->getAckMessageErrorItems() as $errorItem) {
                    printf("\tReceiptHandle:%s, ErrorCode:%s, ErrorMsg:%s\n", $errorItem->getReceiptHandle(), $errorItem->getErrorCode(), $errorItem->getErrorCode());
                }
                //return;
            }
            //throw $e;
        }
    }

    public function __get($name)
    {
        if (!in_array($name, $this->allowParams)) {
            throw new \Exception("context get invalid param: {$name}");
        }
        return Context::get(__CLASS__ . ':' . $name);
    }

    public function __set($name, $value)
    {
        if (!in_array($name, $this->allowParams)) {
            throw new \Exception("context set invalid param {$name}}");
        }
        return Context::set(__CLASS__ . ':' . $name, $value);
    }
}
