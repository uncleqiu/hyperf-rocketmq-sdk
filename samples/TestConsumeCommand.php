<?php

declare(strict_types=1);

namespace App\Command;

use Hyperf\Command\Command as HyperfCommand;
use Hyperf\Command\Annotation\Command;
use Psr\Container\ContainerInterface;
use Uncleqiu\RocketMQ\Client;

#[Command]
class TestConsumeCommand extends HyperfCommand
{
    public function __construct(protected ContainerInterface $container)
    {
        parent::__construct('consume:topic_one');
    }

    public function configure()
    {
        parent::configure();
        $this->setDescription('Consume the rocketmq data');
    }

    public function handle()
    {
        $this->line('Begin Consume....', 'info');
        (new Client())->consume('topic_key1', $this);
    }

    // rocketmq data consume processing logic
    public function handlerMessage($message)
    {
        $mqData = json_decode($message->getMessageBody(), true);
        // write your consume logic......
        var_dump($mqData);
    }
}