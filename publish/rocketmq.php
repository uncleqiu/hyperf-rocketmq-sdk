<?php

declare(strict_types=1);

use Hyperf\Guzzle\PoolHandler;

/**
 * rocketmq配置文件
 */
return [
    'default' => [ // 默认
        'driver' => 'rocketmq',
        'host' => env('ROCKET_MQ_HTTP_ENDPOINT'), // HTTP协议客户端接入点，进入消息队列RocketMQ版控制台实例详情页面的接入点区域查看
        'accessId' => env('ROCKET_MQ_ACCESS_ID'), // AccessKey ID，阿里云身份验证标识
        'secretKey' => env('ROCKET_MQ_SECRET_KEY'), // AccessKey Secret，阿里云身份验证密钥
        'instanceId' => env('ROCKET_MQ_INSTANCE_ID'), // Topic所属的实例ID，在消息队列RocketMQ版控制台创建
        'guzzleHandler' => PoolHandler::class, // guzzle 连接池
        'options' => [ // 连接池option
            'min_connections' => 10, // 连接池最小连接数
            'max_connections' => 100, // 连接池最大连接数
            'connect_timeout' => 3, // 连接等待超时时间
            'wait_timeout' => 30, // 超时时间
            'heartbeat' => -1, // 心跳
            'max_idle_time' => 60, // 最大闲置时间
        ],
        'topics' => [ // topic集合
            'topic_key1' => [ // topic标识（包含topic信息）
                'topic' => 'topic_name_one', // topic名称
                'groupId' => 'GID_ONE', // Group ID
                'waitSeconds' => '3', // 长轮询时间3秒（最多可设置为30秒）
                'numOfMessages' => '10', // 一次最多消费10条(最多可设置为16条)
                'delay' => 0, // 设置延迟时间
            ],
            'topic_key2' => [
                'topic' => 'topic_name_two',
                'groupId' => 'GID_TWO',
                'waitSeconds' => '3',
                'numOfMessages' => '10',
                'delay' => 0,
            ],
        ],
    ],
];